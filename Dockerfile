FROM maven:3.8.1-jdk-11-openj9

WORKDIR /app

COPY ./src/main ./src/main
COPY ./pom.xml ./system.properties ./

EXPOSE 8080

RUN mvn package

CMD [ "java","-jar","target/ter23-0.0.1-SNAPSHOT.jar"]